package asia.tcrs.mtc.ic2;
import pw.tcrs.tcrscore.sum.IMOD;
import pw.tcrs.tcrscore.sum.SUM_GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ic2.api.item.Items;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import asia.tcrs.mtc.ItemStacks;
import asia.tcrs.mtc.APMachine.APMachineRecipes;
import asia.tcrs.mtc.ATable.ATRecipe;
import asia.tcrs.mtc.ATable.recipe.AlchemyCraftingManager;
import asia.tcrs.mtc.api.MTCAPI;
@Mod(modid = "MTC_IC2", name = "MTCIC2", version = "Beta1.6", dependencies = "required-after:TcrsCore2.5;required-after:tcrs_materialconverter;required-after:IC2")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class MTC_IC2 implements IMOD{
	@Override
	@EventHandler
	public void preload(FMLPreInitializationEvent Event) {}
	@Override
	public void registerItem() {}
	@Override
	@EventHandler
	public void load(FMLInitializationEvent event) {
		ItemStack tinIngot=Items.getItem("tinIngot");
		ItemStack tinOre=Items.getItem("tinOre");
		ItemStack copperIngot=Items.getItem("copperIngot");
		ItemStack copperOre=Items.getItem("copperOre");
		ItemStack Materialstone=ItemStacks.Materialstone;
		ItemStack iridiumOre=Items.getItem("iridiumOre");
		APMachineRecipes.smelting().addSmelting(tinIngot.itemID,1, Items.getItem("cell").splitStack(4), 0);
		SUM_GameRegistry.addShapelessRecipe(tinIngot.splitStack(3), Item.ingotIron, Item.ingotIron, Item.ingotIron, Materialstone);
		SUM_GameRegistry.addShapelessRecipe(new ItemStack(Item.ingotIron,2), tinIngot, tinIngot, Materialstone);
		SUM_GameRegistry.addShapelessRecipe(tinOre.splitStack(2), Block.oreIron, Block.oreIron, Materialstone);
		SUM_GameRegistry.addShapelessRecipe(new ItemStack(Block.oreIron), tinOre, tinOre, Materialstone);
		AlchemyCraftingManager.getInstance().addShapelessRecipe(tinIngot.splitStack(2), Item.ingotIron, Item.ingotIron);
		AlchemyCraftingManager.getInstance().addShapelessRecipe(new ItemStack(Item.ingotIron), tinIngot, tinIngot);
		AlchemyCraftingManager.getInstance().addShapelessRecipe(tinOre.splitStack(2), Block.oreIron, Block.oreIron);
		AlchemyCraftingManager.getInstance().addShapelessRecipe(new ItemStack(Block.oreIron), tinOre, tinOre);
		AlchemyCraftingManager.getInstance().addShapelessRecipe(
				new ItemStack(Block.oreDiamond), tinOre,
				tinOre, tinOre,tinOre, tinOre,
				tinOre, tinOre,tinOre, tinOre,
				tinOre, tinOre,tinOre, tinOre,
				tinOre, tinOre,tinOre);
		SUM_GameRegistry.addShapelessRecipe(copperIngot.splitStack(2), tinIngot, Materialstone);
		SUM_GameRegistry.addShapelessRecipe(tinIngot, copperIngot,copperIngot, Materialstone);
		AlchemyCraftingManager.getInstance().addShapelessRecipe(copperIngot.splitStack(2), tinIngot);
		AlchemyCraftingManager.getInstance().addShapelessRecipe(tinIngot, copperIngot,copperIngot);
		SUM_GameRegistry.addShapelessRecipe(iridiumOre, ItemStacks.ExtraObsidian,ItemStacks.ExtraObsidian,ItemStacks.ExtraObsidian,ItemStacks.ExtraObsidian,Materialstone);
		}
}